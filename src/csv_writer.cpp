//
// C++ Implementation: csv_writer
//
// Description: 
//
//
// Author: Nicolas Guilbert <nicolas@ange.dk>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "csv_writer.h"
#include "spreadsheet.h"
#include "errors.h"
//#include "zip_archive.h"
//#include "sheet.h"
#include "cell.h"

namespace spreadsheet {

csv_writer::csv_writer(const std::string& filename, const Spreadsheet* spreadsheet, int sheet_number, const char separator)
 : m_filename(filename),
   m_spreadsheet(spreadsheet),
   m_sheet_number(sheet_number),
   m_separator(separator)
{
  ofs.open(m_filename.c_str());

  if (!spreadsheet) {
    throw error("spreadsheet was null");
  }
}

void csv_writer::close(){
  ofs.close();
}

csv_writer::~csv_writer(){
  ofs.close();
}

void csv_writer::write_cells_as_row(Sheet::const_iterator begin, Sheet::const_iterator end) {
  for (; begin!=end-1;++begin) {
    ofs << (*begin).text() << m_separator; 
  }
  ofs << (*begin).text();
  ofs << "\n";
}

void csv_writer::write_sheet(){
  const Sheet& sheet = m_spreadsheet->get_sheet(m_sheet_number);
  for (Sheet::const_iterator it = sheet.begin(), rowend=sheet.begin().next_row(), theend=sheet.end();
     it!=theend;
     it.next_row(), rowend.next_row())
  {
    write_cells_as_row(it, rowend);
  }

}

}
