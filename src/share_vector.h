//
// C++ Interface: share_vector
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETSHARE_VECTOR_H
#define SPREADSHEETSHARE_VECTOR_H

#include <vector>
#include <boost/shared_ptr.hpp>

namespace spreadsheet {


/**
  * Like std::vector, but the values are stored as boost::shared_ptr (eventually std::shared_ptr).
  * If a null-element is reference, automatically create a default-created Type and insert it.
  * This class really needs a good, thourough unit test. Use with caution
  * @author Ange Optimization <esben@ange.dk>
*/
template<typename Type,
         typename Policy = default_initializer< typename std::vector<boost::shared_ptr<Type> >::iterator, Type > >
class share_vector{
public:
  typedef std::vector<boost::shared_ptr<Type> > ptr_vector_type;
  typedef typename ptr_vector_type::iterator ptr_vector_type_iterator;
  typedef typename ptr_vector_type::const_iterator ptr_vector_type_const_iterator;
private:
  // Has to be mutable so that default constructed elements can be inserted.
  // So be careful!
  mutable ptr_vector_type m_vec;
  Policy m_policy;
  
public:
  typedef Type value_type;
 
  typedef Type* pointer;
 
  typedef const Type* const_pointer;
 
  typedef Type& reference;
 
  typedef const Type& const_reference;
 
  typedef hidepointer_iterator< typename ptr_vector_type::iterator, Type, Policy> iterator;

  typedef hidepointer_iterator< typename ptr_vector_type::iterator, const Type, Policy> const_iterator;
 
  typedef std::reverse_iterator< const_iterator > const_reverse_iterator;
 
  typedef std::reverse_iterator< iterator > reverse_iterator ;
 
  typedef typename ptr_vector_type::size_type size_type;
 
  typedef typename ptr_vector_type::difference_type difference_type;
 
  typedef typename ptr_vector_type::allocator_type allocator_type;

  /**
   * Default constructor creates no elements.  
   */
  share_vector(Policy policy = Policy()) : m_vec(), m_policy(policy) {}

  /**
    * Create a vector with copies of value
    */
  share_vector(size_type n, const value_type &value, Policy policy = Policy()) {
    for (ptr_vector_type_iterator it=m_vec.begin(), theend=m_vec.end(); it != theend;++it) {
      *it = boost::shared_ptr<Type>(new Type(value));
    }

  }

  /**
   * Create a vector with default elements.
   */
  share_vector (size_type n, Policy policy = Policy()) : m_vec(n), m_policy(policy) {}

  /**
   * Create a vector that is a (shallow) copy of x
   */
  share_vector(const share_vector &x) : m_vec(x.m_vec), m_policy(x.m_policy) {}

 ~share_vector () {}

  share_vector & operator= (const share_vector &x) {
    m_vec = x.m_vec;
    return *this;
  }

  ptr_vector_type& get_shared_ptr_vector() { return m_vec; }

  const ptr_vector_type& get_shared_ptr_vector() const { return m_vec; }


  void assign (size_type n, const Type& value) {
    share_vector(n, value).swap();
  }

  iterator begin() { return iterator(m_vec.begin(), m_policy); }

  const_iterator begin () const { return const_iterator(m_vec.begin(), m_policy); }
 
  iterator end () { return iterator(m_vec.end(), m_policy); }
 
  const_iterator end () const { return const_iterator(m_vec.end(), m_policy); }
 
  reverse_iterator rbegin () { return reverse_iterator(end()); }
 
  const_reverse_iterator rbegin () const { return const_revere_iterator(end()); }
  
  reverse_iterator rend () { return reverse_iterator(begin()); }
  
  const_reverse_iterator rend () const  { return const_reverse_iterator(begin()); }
  
  size_type size () const  { return m_vec.size(); }
  
  size_type max_size () const  { return m_vec.max_size(); }
  
  void resize (size_type new_size, const value_type &value) {
      if (new_size <= size()) {
        resize(new_size);
      } else {
        reserve(new_size); // make sure it doesn't get invalidated during resize
        ptr_vector_type_iterator it = m_vec.begin();
        m_vec.resize(new_size);
        for (ptr_vector_type_iterator theend = m_vec.end(); it != end; ++it) {
          *it = new boost::shared_ptr<Type>(new Type(value));
        }
      }
  }

  void resize (size_type new_size) {
      resize(new_size);
  }

  size_type capacity () const { return m_vec.capacity(); }
  
  bool empty () const  { return m_vec.empty(); }
  
  void reserve (size_type n) { m_vec.reserve(n); }

  reference operator[] (size_type n) {
    boost::shared_ptr<Type>& elem = m_vec[n];
    if (!elem) {
      m_policy(m_vec.begin()+n);
    }
    return *elem;
  }

  void assign(size_type index, typename ptr_vector_type::value_type ptr) {
    m_vec[index] = ptr;
  }

  // Subscript access to the data contained in the vector.  
  const_reference operator[] (size_type n) const {
    boost::shared_ptr<Type>& elem = m_vec[n];
    if (!elem) {
      elem.reset(new Type());
    }
    return *elem;
  }

  // Subscript access to the data contained in the vector.  
  reference at (size_type n) {
    boost::shared_ptr<Type>& elem = m_vec.at(n);
    if (!elem) {
      elem.reset(new Type());
    }
    return *elem;
  }
  // Provides access to the data contained in the vector.  
  const_reference at (size_type n) const {
    boost::shared_ptr<Type>& elem = m_vec.at(n);
    if (!elem) {
      elem.reset(new Type());
    }
    return *elem;
  }

  // Provides access to the data contained in the vector.  
  reference front () { return at(0); }
  
  const_reference front () const  { return at(0); }
  
  reference back () { return at(size()-1); }
  
  const_reference back () const  { return at(size()-1); }
  
  /**
   * Add data to the end of the vector.
   * @param x  (which is copied)
   */
  void push_back (const value_type &x) {
    m_vec.push_back(boost::shared_ptr<Type>(new value_type(x)));
  }

  /**
   * Add data to the end of the vector.
   * @param x (this takes ownership of x.)
   * Use the const reference variant for copy
   */
  void push_back (pointer x) {
    m_vec.push_back(boost::shared_ptr<Type>(x));
  }

  /**
   * Add data to the end of the vector.
   * @param x
   */
  void push_back (const boost::shared_ptr<Type>& x) {
    m_vec.push_back(x);
  }

  /**
   * Removes last element.
   */
  void pop_back () { m_vec.pop_back(); }

  /**
   * Inserts given value into vector before specified iterator.  
   * @param position 
   * @param x 
   * @return iterator pointing to x (old iterator might be invalid)
   */
  iterator insert (iterator position, const value_type &x) {
    return insert(boost::shared_ptr<Type>(new Type(x)));
  }

  /**
   * Inserts given value into vector before specified iterator.  
   * @param position 
   * @param x (ownership taken)
   * @return iterator pointing to x (old iterator might be invalid)
   */
  iterator insert (iterator position, pointer x) {
    return insert(boost::shared_ptr<Type>(x));
  }

  /**
   * Inserts given value into vector before specified iterator.  
   * @param position 
   * @param x 
   * @return iterator pointing to x (old iterator might be invalid)
   */
  iterator insert (iterator position, const boost::shared_ptr<Type>& x) {
    // not very efficient
    return iterator(m_vec(insert(position.m_it)));
  }

  /**
   * Inserts a number of copies of given data into the vector.
   * TODO: Not implemented.
   * @param position 
   * @param n 
   * @param x 
   */
  void insert(iterator position, size_type n, const value_type &x);


  // TODO: Not implemented
  // Inserts a range into the vector.  
  template<typename InputIterator> void insert (iterator position, InputIterator first, InputIterator last) ;

  /**
   * 
   * @param position 
   * @return iterator pointing after deleted element
   */
  iterator erase (iterator position) {
    return iterator(m_vec.erase(position.m_it));
  }

  /**
   * Remove a range of elements.  
   * @param first 
   * @param last 
   * @return iterator pointing past deleted range
   */
  iterator erase (iterator first, iterator last) {
    return iterator(m_vec.erase(first.m_it, last.m_it));
  }

  /**
   * Swaps data with x
   * @param x 
   */
  void swap (share_vector &x) {
    m_vec.swap(x.m_vec);
  }


  /**
   * Delete all contents and set size to 0
   */
  void clear () { m_vec.clear(); }

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
