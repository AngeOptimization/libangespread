//
// C++ Implementation: spreadsheet
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "spreadsheet.h"
#include "sheet.h"
#include "style.h"
#include <boost/foreach.hpp>

#include <sstream>

using namespace std;
using namespace boost;

namespace spreadsheet {

Spreadsheet::Spreadsheet()
{
}


Spreadsheet::~Spreadsheet()
{
}

/*!
    \fn Spreadsheet::addSheet
 */
Sheet& Spreadsheet::add_sheet(const std::string& name)
{
  boost::shared_ptr<Sheet> sheet = Sheet::create(name);
  sheets.push_back(sheet);
  return *sheet;
}

/*!
    \fn spreadsheet::Spreadsheet::getSheet(int index) const
 */
Sheet& Spreadsheet::get_sheet(int index) const
{
  return *sheets[index];
}

std::string spreadsheet::Spreadsheet::default_name() const
{
  ostringstream os;
  os << "Sheet " << num_sheets();
  return os.str();
}

boost::shared_ptr< Sheet > spreadsheet::Spreadsheet::get_sheet_ptr(int index) const
{
  return sheets[index];
}


boost::shared_ptr< style > Spreadsheet::get_style(const string& name)
{
  boost::shared_ptr<style> pstyle = m_style_map[name];
  if (!pstyle) {
    pstyle.reset(new style());
  }
  return pstyle;
}

boost::shared_ptr< style > Spreadsheet::get_style(const string & name) const
{
  style_map_t::const_iterator it = m_style_map.find(name);
  if (it != m_style_map.end()) {
    return it->second;
  }
  return boost::shared_ptr< style >();

}

std::vector< std::string > spreadsheet::Spreadsheet::style_names() const
{
  vector<string> rv;
  rv.reserve(m_style_map.size());
  BOOST_FOREACH(style_map_t::const_reference entry, m_style_map) {
    rv.push_back(entry.first);
  }
  return rv;
}


boost::shared_ptr<style> Spreadsheet::new_style(const std::string & name)
{
  boost::shared_ptr<style> rv(new style());
  m_style_map[name] = rv;
  return rv;
}
} // end namespace


/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
