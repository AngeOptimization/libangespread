//
// C++ Interface: hidepointer_iterator
//
// Description: An iterator that hides the fact that the collection is really
// a collection of shared_ptr<> (or another pointer-like class). Note that the
// pointers are assumed to never be null, with undefined results if they are
// anyway
//
// Note for the C++ novice: This class isn't important. It just hides an implemental
// detail, so that a class that encapsulas a vector of pointers can simulate being
// a container of the pointed-to objects.
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETHIDEPOINTER_ITERATOR_H
#define SPREADSHEETHIDEPOINTER_ITERATOR_H

#include <iterator>
#include <boost/shared_ptr.hpp>

namespace spreadsheet {

  /**
   * Initializes elements by using the default construct when neccessary
   */
  template<typename Iterator, typename T>
  struct default_initializer {
    typename Iterator::reference operator()(Iterator ref) const {
      *ref = typename Iterator::value_type(new T());
      return *ref;
    }
  };

  /**
   * Does nothing at all (leaves new elements as nulls; undefined behaviour if accessed)
   */
  template<typename Iterator>
  struct null_initializer {
    typename Iterator::reference operator()(Iterator ref) const {
      return *ref;
    }
  };

  /**
   @author Ange Optimization <esben@ange.dk>
  */
  // TODO: Incomplete. Many of the friend
  // Adapts a iterator to a shared_ptr collection to a direct iterator, assuming no nulls.
  template < typename Iterator, typename T, typename Initializer = default_initializer<Iterator, T> >
  class hidepointer_iterator : public std::iterator<std::random_access_iterator_tag, T> {
      Iterator m_it;
      Initializer m_initializer;
    public:
      explicit hidepointer_iterator(const Iterator& it, Initializer initializer = Initializer())
          : m_it(it), m_initializer(initializer) {}

      T& operator*() const {
        return !(*m_it) ? *m_initializer(m_it) : **m_it;
      }
      T* operator->() const {
        return &this->operator*();
      }
      hidepointer_iterator operator++() {
        ++m_it;
        return *this;
      }
      hidepointer_iterator operator++(int) {
        hidepointer_iterator tmp(*this);
        ++m_it;
        return tmp;
      }

      hidepointer_iterator operator--()  {
        --m_it;
        return  *this;
      }
      hidepointer_iterator operator--(int) {
        hidepointer_iterator tmp(*this);
        --m_it;
        return tmp;
      }

      T& operator[](const typename Iterator::difference_type& offset) {
        typename Iterator::reference ptr_ref = m_it[offset];
        if (!*m_it) {
          m_initializer(ptr_ref);
        }
        return *ptr_ref;
      }

      hidepointer_iterator& operator+=(const typename Iterator::difference_type& offset) {
        m_it += offset;
        return *this;
      }
      hidepointer_iterator operator+(const typename Iterator::difference_type& offset) {
        return hidepointer_iterator(m_it + offset);
      }
      hidepointer_iterator& operator-=(const typename Iterator::difference_type& offset) {
        m_it -= offset;
        return *this;
      }
      hidepointer_iterator operator-(const typename Iterator::difference_type& offset) {
        return hidepointer_iterator(m_it - offset);
      }

      Iterator get() const {
        return m_it;
      }

      // The friendly comparisons have to be template so that iterators and const_iterator comparison works
      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator==(const hidepointer_iterator<Left_Iterator, S, Q>& lhs,
                                    const hidepointer_iterator<Right_Iterator, S, Q>& rhs);

      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator!=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs);
      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator<(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs);
      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator>(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs);
      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator<=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs);
      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline bool operator>=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs);

      template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
      friend inline typename hidepointer_iterator<Left_Iterator, S, Q>::difference_type
      operator-(const hidepointer_iterator<Left_Iterator, S, Q>& lhs,
                const hidepointer_iterator<Right_Iterator, S, Q>& rhs);

  };

  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator==(const hidepointer_iterator<Left_Iterator, S, Q>& lhs,
                         const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it == rhs.m_it;
  }

  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator!=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it != rhs.m_it;
  }
  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator<(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it < rhs.m_it;
  }
  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator>(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it > rhs.m_it;
  }
  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator<=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it <= rhs.m_it;
  }
  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline bool operator>=(const hidepointer_iterator<Left_Iterator, S, Q>& lhs, const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it >= rhs.m_it;
  }

  template<typename Left_Iterator, typename Right_Iterator, typename S, typename Q>
  inline typename hidepointer_iterator<Left_Iterator, S, Q>::difference_type
  operator-(const hidepointer_iterator<Left_Iterator, S, Q>& lhs,
            const hidepointer_iterator<Right_Iterator, S, Q>& rhs) {
    return lhs.m_it - rhs.m_it;
  }

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
