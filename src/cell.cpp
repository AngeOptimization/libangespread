//
// C++ Implementation: cell
//
// Description:
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cell.h"
#include "text_cell.h"
#include "number_cell.h"
#include "sheet.h"

#include <stdexcept>
#include <iostream>

using namespace std;

namespace spreadsheet {

  Cell::Cell( const Cell_position& cell_position )  : self_reference( cell_position ) {}

  Cell::~Cell() {
  }


  const std::string Cell::empty_string;

  /*!
      \fn Cell::operator=(const std::string& text)
   */
  const Cell& Cell::operator=( const std::string& text ) {
    Text_cell new_cell( self_reference, text );
    return this->operator=(new_cell);
  }

  const Cell& Cell::operator=( double number ) {
    number_cell new_cell( self_reference, number );
    return this->operator=(new_cell);
  }

  const Cell& Cell::operator=( int number ) {
    number_cell new_cell( self_reference, number );
    return this->operator=(new_cell);
  }

  std::string Cell::formula() const {
    return empty_string;
  }

  std::string Cell::text() const {
    return empty_string;
  }

  const Cell& Cell::operator=( const Cell& new_cell ) {
    boost::shared_ptr<Cell> new_cell_copy = new_cell.copy( self_reference );
    boost::shared_ptr<Sheet> sheet = self_reference.m_sheet.lock();
    if ( !sheet ) {
      std::string message(std::string() + "Orphaned cell at " + __PRETTY_FUNCTION__ );
      throw std::runtime_error( message );
    }
    sheet->assigncell( self_reference.m_column,
                       self_reference.m_row,
                       new_cell_copy );
    return *new_cell_copy;
  }

  std::ostream& operator<< ( ostream& stream, const Cell& cell ) {
    return cell.write ( stream );
  }

  std::ostream& Cell::write ( std::ostream& stream ) const {
    return stream << text();
  }


}

/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
