//
// C++ Interface: csv_writer
//
// Description: 
//
//
// Author: Nicolas Guilbert <nicolas@ange.dk>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <iostream>
//#include <sstream>
#include <fstream>
#include "sheet.h"

/**
Dumps single (spread)sheet in a csv file

  @author Ange Optimization <nicolas@ange.dk>
*/

namespace spreadsheet {

class Spreadsheet;
class Cell;

class csv_writer{
    const std::string m_filename;
    const Spreadsheet* m_spreadsheet;
    const int m_sheet_number;
    const char m_separator;

 public:

   /**
     * Create a writer for a single sheet
     * @param os output stream
     * @param spreadsheet a pointer to the spreadsheet object
     * @param sheet_number the index of the sheet to be written
     * @param separator the separator, default is , (comma)
     */
    
    csv_writer(const std::string& filename, const Spreadsheet* spreadsheet, int sheet_number, char separator = '\t');

  /**
   * write sheet
  */
  void write_sheet();
  void close();
  
  ~csv_writer();
  
  private:
  std::ofstream ofs;
  void write_cells_as_row(Sheet::const_iterator begin, Sheet::const_iterator end);

};

}
