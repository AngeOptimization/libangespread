//
// C++ Implementation: cell_position
//
// Description:
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cell_position.h"

#include <iostream>
#include <stdexcept>
#include "sheet.h"

using namespace std;

namespace spreadsheet {

  Cell_position::Cell_position ( boost::shared_ptr<Sheet> sheet, Coord column, Coord row )
    : m_column ( column ),
      m_row ( row )
{
  m_sheet = sheet;
}

  Cell_position::~Cell_position() {
    m_column=0;
    m_row=0;
  }


Sheet& Cell_position::sheet()
{
  if (m_sheet.expired()) {
    throw std::runtime_error("Internal error: m_sheet has expired");
  }
  return *m_sheet.lock();
}

}

/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
