//
// C++ Implementation: zip_archive
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "zip_archive.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <zip.h>
#include <boost/shared_ptr.hpp>
#include <cerrno>
#include <cstring>
#include "errors.h"

using namespace std;
using namespace boost;

namespace {
  bool is_present(const string& pathname) {
    struct stat buf;
    int rv = stat(pathname.c_str(), &buf);
    if (rv==-1) {
      if (errno == ENOENT) {
        return false;
      } else {
        throw spreadsheet::ioerror(pathname, strerror(errno));
      }
    }
    return true;
  }

  bool is_regular_file(const string& pathname) {
    struct stat buf;
    int rv = stat(pathname.c_str(), &buf);
    if (rv==-1) {
      throw spreadsheet::ioerror(pathname, strerror(errno));
    }
    return ((buf.st_mode & S_IFMT) == S_IFREG);
  }

  void delete_file(const string& pathname) {
    int rv = unlink(pathname.c_str());
    if (rv != 0) {
      throw spreadsheet::ioerror(pathname, strerror(errno));
    }
  }


  void delete_if_present(const string& pathname) {
    if (is_present(pathname)) {
      if (is_regular_file(pathname)) {
        delete_file(pathname);
      } else {
        throw spreadsheet::ioerror(pathname, "not a regular file.");
      }
    };
  }

  string get_zip_message(int errorp) {
    char buf[1024];
    zip_error_to_str(&buf[0], sizeof buf, errorp, errorp);
    return string(buf);
  }
}

namespace spreadsheet {

struct zip_archive_data {
  zip* m_zip;
  string m_pathname;
  zip_archive_data(zip* zip_struct, const string& pathname) : m_zip(zip_struct), m_pathname(pathname) {}
  ~zip_archive_data() { if (!m_zip) { zip_close(m_zip); } }
};

zip_archive::zip_archive(zip* zip_struct, const string& pathname)
  : dptr(new zip_archive_data(zip_struct, pathname))
{
}

zip_archive::~zip_archive()
{
}

zip_archive zip_archive::create_overwrite(const std::string& pathname) {
  if (pathname.find('\0') != string::npos) {
    throw ioerror(pathname, "contained binary 0 characters");
  }
  delete_if_present(pathname);
  int errorp;
  zip* zip_struct = zip_open(pathname.c_str(), ZIP_CREATE | ZIP_EXCL, &errorp);
  if (!zip_struct) {
    throw ioerror(pathname, get_zip_message(errorp));
  }
  const char comment[] = "Created by libspreadsheet using libzip";
  int rv = zip_set_archive_comment( zip_struct, comment, sizeof(comment)- 1);
  if (rv != 0) {
    throw ioerror(pathname, zip_strerror(zip_struct));
  }

  return zip_archive( zip_struct, pathname );
  
}

void zip_archive::close() {
  if (dptr->m_zip) {
    int rv = zip_close(dptr->m_zip);
    if (rv == -1) {
      const char* message = zip_strerror(dptr->m_zip);
      throw ioerror(dptr->m_pathname, message);
    }
    dptr->m_zip = 0L;
  }
}

void zip_archive::add_file(const std::string& pathname, const std::string& contents) {
  struct zip_source* src = zip_source_buffer( dptr->m_zip, strdup(contents.c_str()), contents.size(), 1);
  if (!src) {
    throw ioerror(dptr->m_pathname, zip_strerror(dptr->m_zip));
  }
  int rv = zip_add( dptr->m_zip, pathname.c_str(), src);
  if (rv == -1) {
    throw ioerror(dptr->m_pathname, zip_strerror(dptr->m_zip));
  }

}

}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
