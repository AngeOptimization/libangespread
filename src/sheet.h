//
// C++ Interface: sheet
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETSHEET_H
#define SPREADSHEETSHEET_H

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "cell_position.h"
#include "cell_iterator.h"
#include "hidepointer_iterator.h"
#include "share_vector.h"

namespace spreadsheet {
class Cell;
class Column;
class Region;
class cell_iterator;

struct textcell_initializer {
  const Sheet* m_sheet;
  textcell_initializer(const Sheet* sheet) : m_sheet(sheet) {}
  boost::shared_ptr<Cell>& operator()(std::vector<boost::shared_ptr<Cell> >::iterator it) const;
};

struct column_initializer {
  const Sheet* m_sheet;
  column_initializer(const Sheet* sheet) : m_sheet(sheet) {}
  boost::shared_ptr<Column>& operator()(std::vector<boost::shared_ptr<Column> >::iterator it) const;
};

/**
  @author Ange Optimization <esben@ange.dk>
*/
class Sheet {
private:
    typedef share_vector<Cell, textcell_initializer> cell_vector_t;
    typedef share_vector<Column, column_initializer> column_vector_t;
    typedef cell_vector_t::iterator cell_vector_t_iterator;
    typedef cell_vector_t::const_iterator cell_vector_t_const_iterator;
    boost::weak_ptr<Sheet> self_reference;
    cell_vector_t cell_vector; // all the cells (with possible dead-space reserved)
    column_vector_t column_vector;
    cell_vector_t::size_type row_length; // actual length of rows aka no of columns
    cell_vector_t::size_type row_space;  // space between rows in in cell_vector.
    cell_vector_t::size_type no_rows; // number of rows
    std::string m_name; // name of sheet
    

    // Support for the policy/initializer
    friend class textcell_initializer;
    Cell_position to_cell_position(std::vector<boost::shared_ptr<Cell> >::iterator it) const;
    
public:
    typedef Cell_position::Coord Coord;
    typedef cell_iterator_t<cell_vector_t_iterator> iterator;
    typedef cell_iterator_t<cell_vector_t_const_iterator> const_iterator;
    iterator begin() { return iterator(cell_vector.begin(), row_length, row_space-row_length); }
    iterator end() { return iterator(cell_vector.end(), row_length, row_space-row_length); }
    const_iterator begin() const { return const_iterator(cell_vector.begin(), row_length, row_space-row_length); }
    const_iterator end() const { return const_iterator(cell_vector.end(), row_length, row_space-row_length); }

    void invent_empty_cell(const Cell_position& cell_position, boost::shared_ptr<Cell>& cell) const;

    /**
     * 
     * @param new_no_columns 
     * @param new_no_rows 
     */
    void resize(Coord new_no_columns, Coord new_no_rows);

    friend class Region;
    friend class Cell;

    /**
     *
     * @return a default sheet
     */
    static boost::shared_ptr<Sheet> create(const std::string& name);

    /**
     * Destructs sheet.
     */
    ~Sheet();
//     
    /**
     * Returns a cell for column and row. The cell is writeable, so it is possible
     * to assign directly to it:
     *   mysheet(5,4) = TextCell("cheese and cake");
     * if the cell is not currently in the spreadsheet, one will be created.
     * 
     * @param column 
     * @param row 
     * @return the cell for coord and row
     */
    Cell& operator()(Coord column, Coord row);
    /**
     * 
     * @return true if no cells are set in this sheet.
     */
    bool empty() const { return cell_vector.empty(); }

    /**
     * 
     * @return number of rows in the spreadsheet
     */
    Coord rows() const { return no_rows; }

    /**
     * 
     * @return numbre of cols in the spreadsheet.
     */
    Coord cols() const { return row_length; }

    /**
     * 
     * @return name of sheet
     */
    const std::string& name() const { return m_name; }


    void assigncell(boost::shared_ptr<Cell> new_cell);
  private:
    /**
     * Assign the cell new_cell to this position in the sheet. 
     * @param column 
     * @param row 
     * @param new_cell 
     */
    void assigncell(Coord column, Coord row, boost::shared_ptr<Cell> new_cell);

    // helper functions

    cell_vector_t::size_type get_cell_idx(Coord column, Coord row);


    Sheet(const std::string& name);

};


}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
