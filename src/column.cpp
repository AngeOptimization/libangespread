//
// C++ Implementation: column
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "column.h"

namespace spreadsheet {


Column::~Column()
{
}


Column::Column(const Sheet* sheet) : m_sheet(sheet)
{
}

}

/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
