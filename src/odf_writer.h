//
// C++ Interface: odf_writer
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ODF_WRITERODF_WRITER_H
#define ODF_WRITERODF_WRITER_H

#include <string>
#include <sstream>
#include "zip_archive.h"
#include "sheet.h"

namespace spreadsheet {

class Spreadsheet;
class Cell;

/**
Dumps spreadsheet in a odf file

  @author Ange Optimization <esben@ange.dk>
*/
class odf_writer{
    std::string m_filename;
    const Spreadsheet* m_spreadsheet;
    zip_archive m_za;
    bool m_open;
    std::ostringstream m_os;
    void add_metadata();
public:
    /**
     * Create a writer 
     * @param filename 
     * @param spreadsheet 
     */
    odf_writer(const std::string& filename, const Spreadsheet* spreadsheet);

    /**
     * Write entire spreadsheet
     */
    void write_all();

    /**
     *
     */
    void write_sheet(const Sheet& sheet);
    void write_cell(const Cell& cell,int rep);
    void write_cells_as_row(Sheet::const_iterator begin, Sheet::const_iterator end);

    void close();

    ~odf_writer();

 private:

    /** escaping the 5 standard XML entities
     * this function will break or do nasty things if it is 
     * given an UTF-8 encoded string, as in those a character may fill
     * one to four 8 bit chars
     * this function is a fast hack to allow writing port codes like 'S&K'
     */
    std::string xml_escape(std::string text);

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
