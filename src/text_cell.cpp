//
// C++ Implementation: text_cell
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "text_cell.h"
#include <sstream>
#include <ostream>
#include <string>
#include <iostream>


using namespace std;

namespace spreadsheet {

Text_cell::Text_cell(Cell_position cell_position, const std::string& contents)
: Cell(cell_position),
  contents(contents)
{
}


Text_cell::~Text_cell()
{
}

double Text_cell::numeric() const {
  std::istringstream is(contents);
  double rv = Cell::numeric();
  is >> rv; // If conversion fails, rv is unchanged (=nan)
  // Only return result if the entire contents was converted
  return is.eof() ? rv : Cell::numeric(); 
}

int Text_cell::integer(const int def) const {
  std::istringstream is(contents);
  int rv = Cell::integer(def);
  is >> rv; // If conversion fails, rv is unchanged (=def)
  // If the entire cell is a number, return this. Otherwise
  // delegate to superclass (which will return default)
  return is.eof()?rv:Cell::integer(def);
  
}

std::ostream& Text_cell::write(std::ostream& stream) const {
  return stream << contents;
}


}

/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
