//
// C++ Interface: text_cell
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETTEXT_CELL_H
#define SPREADSHEETTEXT_CELL_H

#include <string>
#include "cell_position.h"
#include "cell.h"

namespace spreadsheet {

/**
  @author Ange Optimization <esben@ange.dk>
*/
class Text_cell : public Cell {
public:
    /**
     * 
     * @param contenst contents of the new cell. Default is a blank cell.
     */
    Text_cell(Cell_position cell_position,
              const std::string& contenst = "");

    /**
     * destroy this cell
     */
    virtual ~Text_cell();

    virtual std::string text() const { return contents; }
    virtual std::string formula() const { return contents; }
    virtual double numeric() const;
    virtual int integer(const int def) const;
    virtual std::ostream& write(std::ostream& stream) const;
    virtual boost::shared_ptr<Cell> copy(const Cell_position& cell_position) const {
      return boost::shared_ptr<Cell>(new Text_cell(cell_position, contents));
    }

    type_t type() const { return type_string; }
private:
  std::string contents;
};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
