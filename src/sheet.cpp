//
// C++ Implementation: sheet
//
// Description: 
//   This is sort of a sparse matrix implementation.
// TODO: Tell about sparse matrix implementation details.
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "sheet.h"
#include "cell.h"
#include "column.h"
#include "text_cell.h"
#include <stdexcept>
#include <iostream>
#include <cmath>
#include <cassert>

using namespace std;
using namespace boost;

namespace spreadsheet {

/* static */
boost::shared_ptr<Sheet> Sheet::create(const std::string& name) {
  boost::shared_ptr<Sheet> sheet(new Sheet(name));
  sheet->self_reference = sheet;
  return sheet;
}

Sheet::Sheet(const std::string& name)
 : self_reference(),
   cell_vector(textcell_initializer(this)),
   column_vector(column_initializer(this)),
   row_length(0),
   row_space(0),
   no_rows(0),
   m_name(name)
{
}

Sheet::~Sheet()
{
  assert(self_reference.expired());
}

Sheet::cell_vector_t::size_type Sheet::get_cell_idx(Sheet::Coord column, Sheet::Coord row) {
  if (column>=cols()) {
    resize(column+1, max(row+1, rows()));
  } else if (row>=rows()) {
    resize(cols(), row+1);
  }
  assert(column>=0);
  if (column<0) {
    throw std::invalid_argument("Negative column index: " + column);
  }

  return row_space*row+column;

}

void Sheet::resize(Coord new_no_columns, Coord new_no_rows) {
  cell_vector_t::ptr_vector_type& pvec = cell_vector.get_shared_ptr_vector();
  // row size
  if (new_no_columns!=cols()) {
    if (new_no_columns>row_space) {
      // Calculate a suitable new row_space (grow exponentially
      cell_vector_t::size_type new_row_space = ((new_no_columns * 3)+1) / 2; // Grow by 150%, rounded up
      // Resize cell_vector to reflect this new size, and also add any new columns while we are at it.
      // No need to grow number of rows exponentially, vector does that for us.
      pvec.resize(new_row_space*max(new_no_rows, rows()));

      if (rows()>1) {
        // point to member function, gotta love that syntax
        void (boost::shared_ptr<Cell>::*resetfunc)() = &boost::shared_ptr<Cell>::reset;

        // move the rows (no need to move the first row, which incidently makes the loop simpler)
        // Moving is backwards to eliminate any overwriting of data
        for (cell_vector_t::ptr_vector_type::reverse_iterator row_src_end = (pvec.rend()-(rows()-1)*row_space),
                    row_dest_it = pvec.rend()-(new_row_space*rows());
            row_src_end != pvec.rend();
            row_src_end+=row_space, row_dest_it+=new_row_space)
        {
          // clean up extra space. (This will make cells destruct exactly when they are gone from
          // the sheet, e.g.)
          for_each(row_dest_it, row_dest_it+(new_row_space-cols()), mem_fun_ref(resetfunc));
          // copy data
          copy(row_src_end-cols(), row_src_end, row_dest_it+(new_row_space-cols()));
        }
        // Finally, clear the bit after the first row
        for_each(pvec.begin()+row_length, pvec.begin()+new_row_space, mem_fun_ref(resetfunc));
      }

      // update physical layout
      row_space = new_row_space;
      no_rows = max(new_no_columns, cols());

    }
    row_length = new_no_columns;
  }
  if (rows() != new_no_rows) {
    // Only resize number of rows
    pvec.resize(row_space * new_no_rows);
    no_rows = new_no_rows;
  }
  assert(no_rows * row_space == pvec.size());

}

void Sheet::assigncell(Coord column, Coord row, boost::shared_ptr<Cell> new_cell) {
  cell_vector.assign(get_cell_idx(column, row), new_cell);
}

void spreadsheet::Sheet::assigncell(boost::shared_ptr< Cell > new_cell)
{
  assigncell(new_cell->position().column(), new_cell->position().row(), new_cell);
}

void Sheet::invent_empty_cell(const Cell_position& cell_position, boost::shared_ptr<Cell>& cell) const {
  boost::shared_ptr<Sheet> sheet = self_reference.lock();
  if (!sheet) {
    throw std::runtime_error("Sheet was null, but shouldn't be" + std::string(__PRETTY_FUNCTION__));
  }
  cell.reset(new Text_cell(cell_position));
}

Cell_position Sheet::to_cell_position(std::vector<boost::shared_ptr<Cell> >::iterator it) const {
  std::vector<boost::shared_ptr<Cell> >::const_iterator beg = cell_vector.get_shared_ptr_vector().begin();
  std::vector<boost::shared_ptr<Cell> >::size_type cell_idx = it - beg;
  Coord col = cell_idx % (row_space);
  Coord row = cell_idx / (row_space);
  return Cell_position(self_reference.lock(), col, row);
}


Cell& Sheet::operator()(Coord column, Coord row) {
  return cell_vector[get_cell_idx(column, row)];
}

boost::shared_ptr<Cell>& textcell_initializer::operator()(std::vector<boost::shared_ptr<Cell> >::iterator it) const {
  assert( !(*it) );
  Cell_position cell_position = m_sheet->to_cell_position(it);
  m_sheet->invent_empty_cell( cell_position, *it );
  assert( !!(*it) );
  return *it;
}

boost::shared_ptr<Column>& column_initializer::operator()(std::vector<boost::shared_ptr<Column> >::iterator it) const {
  *it = boost::shared_ptr<Column>(new Column(m_sheet));
  return *it;
}



}

/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
