//
// C++ Interface: column
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETCOLUMN_H
#define SPREADSHEETCOLUMN_H

namespace spreadsheet {

class Sheet;



/**
  @author Ange Optimization <esben@ange.dk>
*/
class Column{
  const Sheet* m_sheet;
public:
    Column(const Sheet* sheet);

    ~Column();

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
