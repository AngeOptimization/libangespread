//
// C++ Implementation: spreadsheet_stream
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "spreadsheet_stream.h"
#include "text_cell.h"
#include "number_cell.h"
#include <string>
#include <sstream>

using namespace std;
using namespace boost;

namespace spreadsheet {

struct spreadsheet_stream_data {
    Spreadsheet m_spreadsheet;
    Cell_position m_cur_pos;
    bool m_brief;
    bool m_concat_mode;
    ostringstream m_buffer;
    std::string m_style_name;

    spreadsheet_stream_data()  :
      m_spreadsheet(),
      m_cur_pos(Cell_position(boost::shared_ptr<Sheet>(), 0,0)),
      m_brief(true),
      m_concat_mode(false),
      m_buffer(),
      m_style_name()
    {
    }
};

newrow_t newrow;
brief_t brief;
verbose_t verbose;
startcell_t startcell;
endcell_t endcell;

spreadsheet_stream::spreadsheet_stream() : dptr(new spreadsheet_stream_data())
{
}


spreadsheet_stream::~spreadsheet_stream()
{
}



void spreadsheet::spreadsheet_stream::sheet(const std::string & name)
{
  dptr->m_spreadsheet.add_sheet(name);
  dptr->m_cur_pos = Cell_position(dptr->m_spreadsheet.get_sheet_ptr(dptr->m_spreadsheet.num_sheets()-1),0,0);
}

spreadsheet_stream& spreadsheet::spreadsheet_stream::operator <<(const std::string & str)
{
  if (concat()) {
    dptr->m_buffer << str;
  } else {
    boost::shared_ptr<Cell> pcell(new Text_cell(dptr->m_cur_pos, str));
    pcell->style_name(dptr->m_style_name);
    dptr->m_cur_pos.sheet().assigncell(pcell);
    ++dptr->m_cur_pos;
  }
  return *this;
}


spreadsheet_stream& spreadsheet_stream::operator <<(int value)
{
  if (concat()) {
    dptr->m_buffer << value;
  } else {
    boost::shared_ptr<Cell> pcell(new number_cell(dptr->m_cur_pos, value));
    pcell->style_name(dptr->m_style_name);
    dptr->m_cur_pos.sheet().assigncell(pcell);
    ++dptr->m_cur_pos;
  }
  return *this;

}

spreadsheet_stream& spreadsheet_stream::operator <<(unsigned int value)
{
  if (concat()) {
    dptr->m_buffer << value;
  } else {
    boost::shared_ptr<Cell> pcell(new number_cell(dptr->m_cur_pos, value));
    pcell->style_name(dptr->m_style_name);
    dptr->m_cur_pos.sheet().assigncell(pcell);
    ++dptr->m_cur_pos;
  }
  return *this;

}


spreadsheet_stream& spreadsheet_stream::operator<<(double value)
{
  if (concat()) {
    dptr->m_buffer << value;
  } else {
    boost::shared_ptr<Cell> pcell(new number_cell(dptr->m_cur_pos, value));
    pcell->style_name(dptr->m_style_name);
    dptr->m_cur_pos.sheet().assigncell(pcell);
    ++dptr->m_cur_pos;
  }
  return *this;
}

spreadsheet_stream& spreadsheet_stream::operator<<(const newrow_t )
{
  if (concat()) {
    concat(false);
  }
  dptr->m_cur_pos.next_row();
  dptr->m_cur_pos.column(0);
  return *this;
}

spreadsheet_stream& spreadsheet_stream::operator<<(const newsheet_t& newsheet )
{
  if (concat()) {
    concat(false);
  }
  if (!brief()){
    brief(true);
  }
  sheet(newsheet.m_name);
  return *this;
}

spreadsheet_stream & spreadsheet_stream::operator <<(const new_style & ns)
{
  boost::shared_ptr<style> pstyle = dptr->m_spreadsheet.new_style(ns.m_style_name);
  pstyle->style_for(ns.m_style_contents, ns.m_exporter);
  return *this;
}

spreadsheet_stream& spreadsheet_stream::operator<<(const spreadsheet::set_style & ss)
{
  this->set_style(ss.m_style_name);
  return *this;
}

bool spreadsheet::spreadsheet_stream::brief() const
{
  return dptr->m_brief;
}

void spreadsheet::spreadsheet_stream::brief(bool brief)
{
  dptr->m_brief = brief;
}

Spreadsheet & spreadsheet::spreadsheet_stream::spreadsheet()
{
  return dptr->m_spreadsheet;
}

const Spreadsheet & spreadsheet::spreadsheet_stream::spreadsheet() const {
  return dptr->m_spreadsheet;
}

bool spreadsheet::spreadsheet_stream::concat() const
{
  return dptr->m_concat_mode;
}

void spreadsheet::spreadsheet_stream::concat(bool concat)
{
  if (dptr->m_concat_mode) {
    dptr->m_concat_mode = false;
    *this << dptr->m_buffer.str();
    dptr->m_buffer.str("");
  }
  dptr->m_concat_mode = concat;
}

void spreadsheet_stream::set_style(const std::string & style_name) {
  dptr->m_style_name = style_name;

}

} // end spreadsheet namespace





/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
