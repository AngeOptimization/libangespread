//
// C++ Interface: cell
//
// Description:
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETCELL_H
#define SPREADSHEETCELL_H

#include <string>
#include <limits>

#include "cell_position.h"

namespace spreadsheet {

  class Cell;

  /**
  A cell in the spreadsheet. E.g. a formula and it's result.

   @author Ange Optimization <esben@ange.dk>
  */
  class Cell {
      static const std::string empty_string;
    public:
      Cell ( const Cell_position& cell_position );

      /**
        destructor. This class has no member variables.
      */
      virtual ~Cell();

      /**
       * put cell into stream. Meant mostly for debugging purposes.
       * Usage is the usual std::cout << mycell << endl; type.
       * @param stream the stream to write to.
       * @param cell the cell that is written.
       */
      friend std::ostream& operator<< ( std::ostream& stream, const Cell& cell );

      /**
       * @return formula for this cell.
       * TODO: Create a real formula object. Even if it just a std::string for now.
       */
      virtual std::string formula() const;

      /**
       * @return this cell's context as a string. E.g, numbers are converted to a string
       */
      virtual std::string text() const;

      /**
       * 
       * @return the cell as a double, or nan if the cell is not (convertible to) double
       */
      virtual double numeric() const {
        return std::numeric_limits<double>::quiet_NaN();
      }

      /**
       * 
       * @param def 
       * @return the integer value of a cell or default if the cell is not an integer
       *   e.g. 34.3 is not an integer, nor is "2   ", but "  3" or -23242 are.
       */
      virtual int integer(const int def) const {
        return def;
      }

      /**
       * 
       * @param new_cell 
       * @return reference to owned copy of new_cell
       */
      const Cell& operator= ( const Cell& new_cell );

      virtual const Cell& operator= ( const std::string& text );
      virtual const Cell& operator= ( int number );
      virtual const Cell& operator= ( double number );

      /**
       * Subclasses should output themselves in a manner that is short and interesting
       * for debugging purposes.
      */
      std::ostream& write ( std::ostream& stream ) const;

      /**
       * 
       * @return a full copy of this cell, but with new position
       */
      virtual boost::shared_ptr<Cell> copy ( const Cell_position& cell_position ) const = 0;

      const Cell_position& position() const { return self_reference; }

      // The cell type. 
      enum type_t { type_string, type_float, type_int }; 
      virtual type_t type() const = 0;

      const std::string& style_name() const { return m_style_name; }
      void style_name(const std::string& style_name) { m_style_name = style_name; }

    private:
      Cell_position self_reference;
      std::string m_style_name;
  };


} // end of namespace

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
