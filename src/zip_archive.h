//
// C++ Interface: zip_archive
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETZIP_ARCHIVE_H
#define SPREADSHEETZIP_ARCHIVE_H

struct zip;

#include <string>
#include <boost/shared_ptr.hpp>

namespace spreadsheet {

class zip_archive_data;

/**
  Encapsulates a zip-archive
  @author Ange Optimization <esben@ange.dk>
*/
class zip_archive{
  private:
    boost::shared_ptr<zip_archive_data> dptr;
    zip_archive(zip* zip_struct, const std::string& pathname);
  public:
    ~zip_archive();

    /**
     * 
     * @param pathname pathname (e.g. mydir/myfile) for file
     * @param contents  the contents
     */
    void add_file(const std::string& pathname, const std::string& contents);
    /**
     * Closes and writes to disk. If fails, throws an ioerror
     */
    void close();

    static zip_archive create_overwrite(const std::string& pathname);

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
