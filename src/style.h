//
// C++ Interface: style
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETSTYLE_H
#define SPREADSHEETSTYLE_H

#include <string>

namespace spreadsheet {
class Spreadsheet;

/**
  * Holds style information, e.g. boldness, column width, and so on.
  * Constructed using the get_style member function on Spreadsheet

  @author Ange Optimization <esben@ange.dk>
*/
class style{
    style();
    friend class Spreadsheet;
    std::string m_style_for_odf; // TODO: Make this a map, when the need arises
public:

    enum exporter_t { exporter_odf }; // TODO: Move to some superclass of writers
    /**
     * Set style for exporter
     * @param value the style fragment, included in the exported document
     *               a suitable place.
     * @param exporter. (Currently, only odf is available)
     */
    void style_for(const std::string& value, exporter_t exporter = exporter_odf);

    const std::string& style_for(exporter_t exporter = exporter_odf);

    ~style();

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
