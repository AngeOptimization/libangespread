//
// Description: Error classes, for throwing
//
// All inherit from spreadsheet::error
//
// 
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEET_ERROR_H
#define SPREADSHEET_ERROR_H

#include <stdexcept>

namespace spreadsheet {
  class error : public std::runtime_error {
    public:
      error(const std::string& message) : runtime_error(message) {}
  };

  class ioerror : public error {
    public:
      ioerror(const std::string& pathname, const std::string& message) :
        error("IO error while accessing \""+pathname+"\": "+message) {}
    
  };
}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
