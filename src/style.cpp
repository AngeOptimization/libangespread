//
// C++ Implementation: style
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "style.h"

namespace spreadsheet {


style::style() : m_style_for_odf()
{
}


style::~ style()
{
}
}

void spreadsheet::style::style_for(const std::string & value, exporter_t exporter)
{
  m_style_for_odf = value;
}

const std::string & spreadsheet::style::style_for(exporter_t exporter)
{
  return m_style_for_odf;
}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
