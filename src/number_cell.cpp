//
// C++ Implementation: number_cell
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "number_cell.h"
#include <sstream>
#include <iomanip>
#include <cmath>
#include <ostream>

using namespace std;

namespace spreadsheet {


number_cell::number_cell(Cell_position cell_position, double contents)
: Cell(cell_position),
  m_contents(contents)
{
  m_is_int = false;
}

number_cell::number_cell(Cell_position cell_position, int contents)
: Cell(cell_position),
  m_contents(contents)
{
  m_is_int = true;
}
number_cell::number_cell(Cell_position cell_position, unsigned int contents)
: Cell(cell_position),
  m_contents(contents)
{
  m_is_int = true;
}


number_cell::~number_cell()
{
}

std::string number_cell::text() const {
  std::ostringstream os;
  if (m_is_int) {
    os << static_cast<int>(m_contents);
  } else {
    locale mylocale("C");
    os.imbue(mylocale);
    // An IEEE double has 53 significant bits, approximately 15.95 decimal digits (log10(2^53)), using 15
    // Don't use fixed modifier, as we do not want to have a lot of trailing zeros at end of number print
    // or arbitrarily invented trailing digits
    os << setprecision(15) << m_contents;
  }
  return os.str();
}

std::string number_cell::formula() const {
  return text();
}

int number_cell::integer(const int def) const {
  return static_cast<int>(round(m_contents));
  
}

std::ostream& number_cell::write(std::ostream& stream) const {
  return stream << m_contents;
}

}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
