//
// C++ Interface: cell_position
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETCELL_POSITION_H
#define SPREADSHEETCELL_POSITION_H

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

namespace spreadsheet {

class Cell;
class Sheet;


/**
Full cell position

  @author Ange Optimization <esben@ange.dk>
*/
class Cell_position {
public:
    typedef unsigned Coord;
    Cell_position(boost::shared_ptr<Sheet> sheet, Coord column, Coord row);

    ~Cell_position();

    Sheet& sheet();
    Coord column() const { return m_column; }
    Coord row() const { return m_row; }
    void column(Coord column) { m_column=column; }
    void row(Coord row) { m_row=row; }
    const Cell_position& operator++() { ++m_column; return *this; }
    const Cell_position& next_row() { ++m_row; m_column=0; return *this; }
    

private:
    boost::weak_ptr<Sheet> m_sheet;
    Coord m_column;
    Coord m_row;

    friend class Cell;
    friend class Sheet;
};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
