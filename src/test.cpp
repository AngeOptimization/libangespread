//
// C++ Implementation: test
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

//#define NO_BASIC_TESTS
//#define NO_ITERATOR_TESTS
//#define NO_CSV_TESTS
//#define NO_ODF_TESTS

#include "spreadsheet.h"
#include "sheet.h"
#include "cell.h"
#include "csv_parser.h"
#include "odf_writer.h"
#include "errors.h"

#include <cstring>
#include <sstream>
#include <iostream>
#include <cerrno>
#include <cmath>

using namespace spreadsheet;
using namespace std;
template<typename T, typename S>
void check(const T& actual, const S& expected, const string& error_text, int& error) {
  if (actual != expected) {
    error++;
    cerr << error_text << "; actual=<" << actual << ">;  expected=<" << expected  << ">" << endl;
  }
}

template<>
void check(const double& actual, const double& expected, const string& error_text, int& error) {
  if (actual != expected && !(isnan(actual) && isnan(expected))) {
    error++;
    cerr << error_text << "; actual=<" << actual << ">;  expected=<" << expected  << ">" << endl;
  }
}

template<>
void check(const unsigned& actual, const int& expected, const string& error_text, int& error) {
  if (expected<0 || static_cast<unsigned>(expected) != actual) {
    error++;
    cerr << error_text << "; actual=<" << actual << ">;  expected=<" << expected  << ">" << endl;
  }
}

void basic_tests(int& errors) {
  Spreadsheet spreadsheet;
  Sheet& sheet1 = spreadsheet.add_sheet();
  cout << "Testing automatic resize" << endl;
  sheet1(0,0) = "(0,0)";
  check(sheet1(0,0).text(), string("(0,0)"), "First cell creation check failed", errors);
  sheet1(5,0) = "(5,0)";
  sheet1(3,0) = "(3,0)";
  check(sheet1(5,0).text(), string("(5,0)"), "Column resize check failed", errors);
  check(sheet1(3,0).text(), string("(3,0)"), "Column resize check failed", errors);
  check(sheet1(0,0).text(), string("(0,0)"), "Column resize check failed", errors);
  sheet1(0,5) = "(0,5)";
  sheet1(5,5) = "(5,5)";
  sheet1(3,5) = "(3,5)";
  sheet1(5,3) = "(5,3)";
  sheet1(3,3) = "(3,3)";
  check(sheet1(0,5).text(), string("(0,5)"), "Row resize check failed", errors);
  check(sheet1(5,5).text(), string("(5,5)"), "Row resize check failed", errors);
  check(sheet1(3,5).text(), string("(3,5)"), "Row resize check failed", errors);
  check(sheet1(5,3).text(), string("(5,3)"), "Row resize check failed", errors);
  check(sheet1(3,3).text(), string("(3,3)"), "Row resize check failed", errors);
  check(sheet1(5,0).text(), string("(5,0)"), "Row resize check failed", errors);
  check(sheet1(3,0).text(), string("(3,0)"), "Row resize check failed", errors);
  check(sheet1(0,0).text(), string("(0,0)"), "Row resize check failed", errors);
  sheet1(10,20) = "(10,20)";
  check(sheet1(0,5).text(), string("(0,5)"),   "Double resize check failed", errors);
  check(sheet1(5,5).text(), string("(5,5)"),   "Double resize check failed", errors);
  check(sheet1(3,5).text(), string("(3,5)"),   "Double resize check failed", errors);
  check(sheet1(5,3).text(), string("(5,3)"),   "Double resize check failed", errors);
  check(sheet1(3,3).text(), string("(3,3)"),   "Double resize check failed", errors);
  check(sheet1(5,0).text(), string("(5,0)"),   "Double resize check failed", errors);
  check(sheet1(3,0).text(), string("(3,0)"),   "Double resize check failed", errors);
  check(sheet1(0,0).text(), string("(0,0)"),   "Double resize check failed", errors);
  check(sheet1(10,20).text(), string("(10,20)"), "Double resize check failed", errors);


  {
    Sheet& sheet = spreadsheet.add_sheet();

    cout << "adding 500 rows and 500 columns" << endl;
    for (int column =0; column<500; column++) {
      for (int row=0; row < 500; row++) {
        ostringstream os;
        os << "Row " << row << " and column " << column;
        sheet(column, row) = os.str();
      }
    }

    cout << "checking contents" << endl;
    for (int column =0; column<500; column++) {
      for (int row=0; row < 500; row++) {
        ostringstream os;
        os << "Row " << row << " and column " << column;
        string actual = sheet(column,row).text();
        if (actual != os.str()) {
          cerr << "Expected \"" << os.str() << "\" but got \"" << actual << "\" at " << __LINE__ << endl;
          errors++;
        }
      }
    }
  }

  {
    Sheet& sheet = spreadsheet.add_sheet();
    cout << "adding 10 columns and 15000 rows" << endl;
    for (int row=0; row < 15000; row++) {
      for (int column =0; column<10; column++) {
        ostringstream os;
        os << "Row " << row << " and column " << column;
        sheet(column, row) = os.str();
      }
    }

    cout << "checking contents" << endl;
    for (int column =0; column<10; column++) {
      for (int row=0; row < 15000; row++) {
        ostringstream os;
        os << "Row " << row << " and column " << column;
        string actual = sheet(column,row).text();
        if (actual != os.str()) {
          cerr << "Expected \"" << os.str() << "\" but got \"" << actual << "\" at " << __LINE__ << endl;
          errors++;
        }
      }
    }
  }

  // Test numeric
  cout << "Testing numeric() and integer()" << endl;
  Sheet& numeric = spreadsheet.add_sheet();
  numeric(0,0) = "123";
  numeric(0,1) = "12.58";
  numeric(0,2) = "  12.34";
  numeric(0,3) = "124d";
  numeric(0,4) = "d235";
  numeric(0,5) = "1.2e5";
  numeric(0,6) = "15.32  ";
  numeric(0,7) = "  -54";
  numeric(0,8) = 12;
  numeric(0,9) = 42.52;
  numeric(0,10) = "-";
  numeric(0,11) = ".";
  numeric(0,12) = "   ";


  double nan = std::numeric_limits<double>::quiet_NaN();

  check(numeric(0,0).numeric(), 123.0, "Numeric 1 test failed",errors);
  check(numeric(0,1).numeric(), 12.58, "Numeric 2 test failed",errors);
  check(numeric(0,2).numeric(), 12.34, "Numeric 3 test failed",errors);
  check(numeric(0,3).numeric(), nan, "Numeric 4 test failed",errors);
  check(numeric(0,4).numeric(), nan, "Numeric 5 test failed",errors);
  check(numeric(0,5).numeric(), 1.2e5, "Numeric 6 test failed",errors);
  check(numeric(0,6).numeric(), nan, "Numeric 7 test failed",errors);
  check(numeric(0,7).numeric(), -54.0, "Numeric 8 test failed",errors);
  check(numeric(0,8).numeric(), 12.0, "Numeric 9 test failed",errors);
  check(numeric(0,9).numeric(), 42.52, "Numeric 10 test failed",errors);
  check(numeric(0,10).numeric(), nan, "Numeric 11 test failed",errors);
  check(numeric(0,11).numeric(), nan, "Numeric 12 test failed",errors);
  check(numeric(0,12).numeric(), nan, "Numeric 13 test failed",errors);
  check(numeric(0,0).integer(-42), 123, "Integer test 1 failed",errors);
  check(numeric(0,1).integer(-43), -43, "Integer test 2 failed",errors);
  check(numeric(0,2).integer(-44), -44, "Integer test 3 failed",errors);
  check(numeric(0,3).integer(-45), -45, "Integer test 4 failed",errors);
  check(numeric(0,4).integer(-47), -47, "Integer test 5 failed",errors);
  check(numeric(0,5).integer(-48), -48, "Integer test 6 failed",errors);
  check(numeric(0,6).integer(-49), -49, "Integer test 7 failed",errors);
  check(numeric(0,7).integer(-50), -54, "Integer test 8 failed",errors);
  check(numeric(0,8).integer(-52), 12, "Integer test 9 failed", errors);
  check(numeric(0,9).integer(-54), 43, "Integer test 10 failed", errors);
  check(numeric(0,10).integer(-55), -55, "Integer test 11 failed",errors);
  check(numeric(0,11).integer(-56), -56, "Integer test 12 failed",errors);
  check(numeric(0,12).integer(-57), -57, "Integer test 13 failed",errors);

}

struct my_count {
  int* m_c;
  my_count(int* c) : m_c(c) {}
  int operator()() {
    return (*m_c)++;
  }
};

void iterator_tests(int& errors) {
  int olderror = errors;

  cout << "Testing basic iterator functions" << endl;

  Spreadsheet spreadsheet;
  Sheet& sheet1 = spreadsheet.add_sheet();
  sheet1.resize(10,10);
  int c = 0;
  generate(sheet1.begin(), sheet1.end(), my_count(&c));
  check(c, 100, "Basic filling failed", errors);
  check(sheet1.rows(), 10, "Basic filling size failed", errors);
  check(sheet1.cols(), 10, "Basic filling size failed", errors);
  check(sheet1(0,0).integer(-1), 0, "Basic filling failed", errors);
  check(sheet1(9,0).integer(-1), 9, "Basic filling failed", errors);
  check(sheet1(0,1).integer(-1),10, "Basic filling failed", errors);
  check(sheet1(3,6).integer(-1), 63, "Basic filling failed", errors);
  check(sheet1(6,3).integer(-1), 36, "Basic filling failed", errors);
  check(sheet1(9,9).integer(-1), 99, "Basic filling failed", errors);

  c = 0;
  my_count mcnt(&c);
  for (Sheet::iterator it = sheet1.begin(), theend=sheet1.end(); it != theend; ++it) {
    check(it->integer(-1), mcnt(), "Scan through failed", errors);
  }

  cout << "Testing striding" << endl;

  Sheet::iterator::difference_type stride = 7;
  Sheet::iterator it1 = sheet1.begin();
  Sheet::iterator it2 = sheet1.end();
  for (int i=0; i<10; i++) {
    sheet1.begin()[i*stride+1]=i*100;
    sheet1.end()[-i*stride-1]=-i*100;
    *it1 = i;
    it1+=stride;
    it2-=stride;
    *it2 = -i;

  }
  check(sheet1(7,0).integer(-1), 1, "Striding failed", errors);
  check(sheet1(8,0).integer(-1), 100, "Striding failed", errors);
  check(sheet1(4,1).integer(-1), 2, "Striding failed", errors);
  check(sheet1(5,1).integer(-1), 200, "Striding failed", errors);
  check(sheet1(1,2).integer(-1), 3, "Striding failed", errors);
  check(sheet1(2,2).integer(-1), 300, "Striding failed", errors);
  check(sheet1(3,6).integer(-1), 9, "Striding failed", errors);
  check(sheet1(4,6).integer(-1), 900, "Striding failed", errors);
  check(sheet1(3,9).integer(42), 0, "Striding failed", errors);
  check(sheet1(9,9).integer(42), -0, "Striding failed", errors);
  check(sheet1(6,8).integer(42), -1, "Striding failed", errors);
  check(sheet1(2,9).integer(42), -100, "Striding failed", errors);
  check(sheet1(9,7).integer(42), -2, "Striding failed", errors);
  check(sheet1(5,8).integer(42), -200, "Striding failed", errors);
  check(sheet1(0,3).integer(42), -9, "Striding failed", errors);
  check(sheet1(6,3).integer(42), -900, "Striding failed", errors);

  it1 = sheet1.begin();
  it2 = sheet1.end()-1;
  for (int i=10; i<20; i++) {
    *it1=i;
    *it2=-i;
    it1.next_row();
    it2.back_pedal_rows(1);
  }
  check(sheet1(0,0).integer(-1), 10, "Row walking failed", errors);
  check(sheet1(9,9).integer(-1), -10, "Row walking failed", errors);
  check(sheet1(0,1).integer(-1), 11, "Row walking failed", errors);
  check(sheet1(9,8).integer(-1), -11, "Row walking failed", errors);
  check(sheet1(0,5).integer(-1), 15, "Row walking failed", errors);
  check(sheet1(9,4).integer(-1), -15, "Row walking failed", errors);
  check(sheet1(0,9).integer(-1), 19, "Row walking failed", errors);

  it1 = sheet1.begin()+2;
  it2 = sheet1.end()-3;
  for (int i=20; i<30; i+=2) {
    *it1=i;
    *it2=-i;
    it1.advance_rows(2);
    it2.back_pedal_rows(2);
  }
  check(sheet1(2,0).integer(-1), 20, "Row striding failed", errors);
  check(sheet1(2,2).integer(-1), 22, "Row striding failed", errors);
  check(sheet1(2,4).integer(-1), 24, "Row striding failed", errors);
  check(sheet1(2,6).integer(-1), 26, "Row striding failed", errors);
  check(sheet1(2,8).integer(-1), 28, "Row striding failed", errors);
  check(sheet1(7,9).integer(-1), -20, "Row striding failed", errors);
  check(sheet1(7,7).integer(-1), -22, "Row striding failed", errors);
  check(sheet1(7,5).integer(-1), -24, "Row striding failed", errors);
  check(sheet1(7,3).integer(-1), -26, "Row striding failed", errors);
  check(sheet1(7,1).integer(-1), -28, "Row striding failed", errors);


  if(olderror == errors) {
    cout << "Iterator tests passed" << endl;
  }
}

void csv_tests(int& errors) {
  int olderror = errors;
  string filename = "data.csv";
  cout << "Loading sheet" << endl;
  auto_ptr<Spreadsheet> spreadsheet = Csv_parser::parse_csv_file(filename);
  cout << "Sheet parsed, checking the result" << endl;
  if (!spreadsheet.get()) {
    cerr << "Failed to open \"" << filename << "\":" << strerror(errno) << endl;
    errors++;
    return;
  }

  check(spreadsheet->num_sheets(), 9, "Number of sheets doesn't match, expected 9", errors);
  Sheet& sheet1 = spreadsheet->get_sheet(0);
  check(sheet1(4,30).text(), string("Tanjung Pelepas"), "Contents of cell E31 on sheet 1  doesn't match", errors);
  check(sheet1(5,2608).text(), string("355.5"), "Contents of cell F2609 on sheet 1  doesn't match",errors);
  check(sheet1(7,14194).text(), string("37129.8134615385"), "Contents of cell H14195 on sheet 1 doesn't match",errors);
  Sheet& sheet5 = spreadsheet->get_sheet(4);
  check(sheet5(5,82).text(), string("Portugal"), "Contents of cell F83 on sheet 5 doesn't match", errors);
  if(olderror == errors) {
    cout << "CSV tests passed" << endl;
  }
}

void odf_tests(int& errors) {
  try {
    cout << "Testing odf. Creating test spreadsheet" << endl;
    Spreadsheet spreadsheet;
    // Test big sheet
    {
      Sheet& sheet = spreadsheet.add_sheet("20x20");

      for (int column =0; column<20; column++) {
        for (int row=0; row < 20; row++) {
          ostringstream os;
          os << "Row " << row << " and column " << column;
          sheet(column, row) = os.str();
        }
      }
    }
    // Test completely empty sheet
    spreadsheet.add_sheet();

    // Test sheet with lots of empty space
    {
      Sheet& sheet = spreadsheet.add_sheet("ostensheet");
      sheet(2,3) = "osten";
    }
    odf_writer writer("test.odf", &spreadsheet);
    cout << "write sheet" << endl;
    writer.write_all();
  } catch (ioerror& ioe) {
    cerr << "Test failed with exception: " << ioe.what() << endl;
    errors++;
  }
}

int main() {
  int errors = 0;

#ifndef NO_BASIC_TESTS
  basic_tests(errors);
#endif

#ifndef NO_ITERATOR_TESTS
  iterator_tests(errors);
#endif

#ifndef NO_CSV_TESTS
  csv_tests(errors);
#endif

#ifndef NO_ODF_TESTS
  odf_tests(errors);
#endif

  if (errors) {
    cout << "Test FAILED, " << errors << " errors. " << endl;
    return 1;
  } else {
    cout << "Test PASSED" << endl;
    return  0;
  }


}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
