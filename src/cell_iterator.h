//
// C++ Interface: cell_iterator
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETCELL_ITERATOR_H
#define SPREADSHEETCELL_ITERATOR_H

#include <iterator>
class Cell;

namespace spreadsheet {

/**
Iterator for cells in a sheet.
A specialized iterator is needed because we have to skip
the extra space at the end of each row...partly due to the
fact that the sheet reserves space there, and partly to support
regions

  @author Ange Optimization <esben@ange.dk>
*/
template<typename Iterator>
class cell_iterator_t : public std::iterator<std::random_access_iterator_tag, typename Iterator::value_type>
{
  Iterator m_it;
  typename Iterator::difference_type m_row_length; // Length of row data
  typename Iterator::difference_type m_row_stride; // Length of non-row-data
  typename Iterator::difference_type m_offset; // Distance advanced into row
public:
  cell_iterator_t(const Iterator& it,
                  typename Iterator::difference_type row_length,
                  typename Iterator::difference_type row_stride,
                  typename Iterator::difference_type offset = 0)
    : m_it(it), m_row_length(row_length), m_row_stride(row_stride), m_offset(offset) {}

  ~cell_iterator_t() {}
  
  typename Iterator::reference operator*() const { return *m_it; }
  typename Iterator::pointer operator->() const { return &(*m_it); }

  cell_iterator_t& next_row() {
    m_it += (m_row_length+m_row_stride);
    return *this;
  }

  cell_iterator_t& advance_rows(typename Iterator::difference_type offset) {
    m_it += (m_row_length+m_row_stride)*offset;
    return *this;
  }

  cell_iterator_t& back_pedal_rows(typename Iterator::difference_type offset) {
    m_it -= (m_row_length+m_row_stride)*offset;
    return *this;
  }

  cell_iterator_t operator++() {
    ++m_it;
    if (++m_offset>=m_row_length) {
      m_offset=0;
      m_it += m_row_stride;
    }
    return *this;
  }

  cell_iterator_t operator++(int) {
    cell_iterator_t tmp(*this);
    ++(*this);
    return tmp;
  }

  cell_iterator_t operator--()  {
    --m_it;
    if (m_offset==0) {
      m_offset=m_row_length;
      m_it -= m_row_stride;
    }
    --m_offset;
    return *this;
  }
  cell_iterator_t operator--(int) {
    cell_iterator_t tmp(*this);
    --(*this);
    return tmp;
  }

  typename Iterator::reference operator[](const typename Iterator::difference_type& offset) {
    // Not the most efficient way, but it works
    cell_iterator_t tmp(*this);
    tmp+=offset;
    return *tmp;
  }

  cell_iterator_t& operator+=(const typename Iterator::difference_type& offset) {
    if (offset<0) {
      (*this)-=-offset;
    } else if (offset>0) {
      m_it+=offset;
      m_offset+=offset;
      while (m_offset>=m_row_length) {
        m_offset-= m_row_length;
        m_it+=m_row_stride;
      }
    }
    return *this;
  }

  cell_iterator_t operator+(const typename Iterator::difference_type& offset) {
    cell_iterator_t tmp(*this);
    tmp+=offset;
    return tmp;
  }

  // FIXME: If m_row_length is 0, this will be an infinite loop.
  cell_iterator_t& operator-=(const typename Iterator::difference_type& offset) {
    if (offset<0) {
      (*this)+=-offset;
    } else if (offset>0) {
      m_it-=offset;
      while (m_offset<offset) {
        m_offset += m_row_length;
        m_it -= m_row_stride;
      }
      m_offset-=offset;
    }
    return *this;
  }

  cell_iterator_t operator-(const typename Iterator::difference_type& offset) {
    cell_iterator_t tmp(*this);
    tmp-=offset;
    return tmp;
  }

  // The friendly comparisons have to be template so that iterators and const_iterator comparison works
  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator==(const cell_iterator_t<Left_Iterator>& lhs,
                                const cell_iterator_t<Right_Iterator>& rhs);

  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator!=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);
  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator<(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);
  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator>(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);
  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator<=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);
  template<typename Left_Iterator, typename Right_Iterator>
  friend inline bool operator>=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);

  template<typename Left_Iterator, typename Right_Iterator>
  friend inline typename cell_iterator_t<Left_Iterator>::difference_type
  operator-(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs);
};

template<typename Left_Iterator, typename Right_Iterator>
typename cell_iterator_t<Left_Iterator>::difference_type operator-(const cell_iterator_t<Left_Iterator>& lhs,
                                                                   const cell_iterator_t<Right_Iterator>& rhs)
{
    typedef typename Left_Iterator::difference_type difference_type;
    difference_type raw_dist = lhs.m_it - rhs.m_it;
    // if the lhs offset is lo, rhs offset is ro, the row_stride is s and the row_length is l
    // we have from raw_dist = N-lo + ro + (q-1)*l + q*s
    // where q is the number of empty spaces we need to subtract to get the real distance.
    // Thus, q = (raw_distance+lo-ro+l)/(s+l)
    difference_type q =
        (raw_dist + lhs.m_offset - rhs.m_offset + lhs.m_row_length) / (lhs.m_row_length+lhs.m_row_stride);
    return raw_dist - q * lhs.m_row_stride;
}


template<typename Left_Iterator, typename Right_Iterator>
inline bool operator==(const cell_iterator_t<Left_Iterator>& lhs,
                              const cell_iterator_t<Right_Iterator>& rhs) {
  return lhs.m_it == rhs.m_it;
}

template<typename Left_Iterator, typename Right_Iterator>
inline bool operator!=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs) { return !(rhs == lhs); }
template<typename Left_Iterator, typename Right_Iterator>
inline bool operator<(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs) {
  return lhs.m_it < rhs.m_it;
}

template<typename Left_Iterator, typename Right_Iterator>
inline bool operator>(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs) {
  return lhs.m_it > rhs.m_it;
}

template<typename Left_Iterator, typename Right_Iterator>
inline bool operator<=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs) {
  return lhs.m_it <= rhs.m_it;
}
template<typename Left_Iterator, typename Right_Iterator>
inline bool operator>=(const cell_iterator_t<Left_Iterator>& lhs, const cell_iterator_t<Right_Iterator>& rhs) {
  return lhs.m_it >= rhs.m_it;
}



}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
