//
// C++ Interface: spreadsheet_stream
//
// Description:
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETSPREADSHEET_STREAM_H
#define SPREADSHEETSPREADSHEET_STREAM_H

#include <boost/shared_ptr.hpp>
#include "cell_position.h"
#include "spreadsheet.h"
#include <string>
#include "sheet.h"
#include <sstream>
#include "style.h"

namespace spreadsheet {

  class Cell;
  class Spreadsheet;
  class spreadsheet_stream_data;


// newrow manipulator
  struct newrow_t {};
  extern newrow_t newrow;

// newsheet manipulator
  struct newsheet_t {
    std::string m_name;
  };

  /**
   * Create a new sheet with name
   * @param name
   */
  inline newsheet_t newsheet(const std::string& name) {
    newsheet_t rv;
    rv.m_name = name;
    return rv;
  }


// brief/verbose toggle manipulator
  struct brief_t {};
  extern brief_t brief;
  struct verbose_t {};
  extern verbose_t verbose;

// startcell and endcell (for concatting into 1 cell)
  struct startcell_t {};
  extern startcell_t startcell;
  struct endcell_t {};
  extern endcell_t endcell;


// style manipulaters
  struct set_style {
    set_style(const std::string& style_name = std::string()) : m_style_name(style_name) {}
    std::string m_style_name;
  };

  struct new_style {
    new_style(const std::string& style_name, const std::string& style_contents, style::exporter_t exporter = style::exporter_odf) :
        m_style_name(style_name),
        m_exporter(exporter),
        m_style_contents(style_contents)

    {}
    std::string m_style_name;
    style::exporter_t m_exporter;
    std::string m_style_contents;

  };

  /**
  Stream for writing spreadsheets.


   @author Ange Optimization <esben@ange.dk>
  */
  class spreadsheet_stream {
      boost::shared_ptr<spreadsheet_stream_data> dptr;
    public:
      /**
       * Construct a stream that adds to spreadsheet
       * Users would want to call sheet(name)
       * before using then stream;
       * @param spreadsheet
       */
      spreadsheet_stream();

      // TODO: implement, not a priority
      spreadsheet_stream(const spreadsheet_stream& other);

      /**
       *
       * @return spreadsheet
       */
      Spreadsheet& spreadsheet();

      /**
       *
       * @return spreadsheet
       */
      const Spreadsheet& spreadsheet() const;


      /**
       * Start a new shete
       * @param name
       */
      void sheet(const std::string& name);

      /**
       *
       * @return whether this stream is currently brief
       */
      bool brief() const;

      /**
       * Sets the "brief" flag. Objects can use this flag to
       * determine how many details should be included.
       *
       * @param brief
       */
      void brief(bool brief);

      /**
       * Whether concat mode, where any operation is appended to the current cell
       */
      bool concat() const;

      /**
       * Enter or exit concat mode, where any operation is appended to current cell.
       * newrow, newsheet ends concat mode automatically.
       * See also the startcell and endcell manipulators
       * @param concat
       */
      void concat(bool concat);

      void set_style(const std::string& style_name);

      ~spreadsheet_stream();

      /**
       * Output a string as a cell
       * @param str
       * @return the stream again (for chaining)
       */
      spreadsheet_stream& operator<<(const std::string& str);

      /**
       * Output an unsigned int as a cell
       * @param value
       * @return the stream again (for chaining)
       */
      spreadsheet_stream& operator<<(unsigned int value);


      /**
       * Output an int as a cell
       * @param value
       * @return the stream again (for chaining)
       */
      spreadsheet_stream& operator<<(int value);

      /**
       * Output a double as a cell
       * @param value
       * @return the stream again (for chaining)
       * @return
       */
      spreadsheet_stream& operator<<(double value);

      /**
       * Start a new row in the current sheet
       * @param
       * @return the stream again (for chaining)
       */
      spreadsheet_stream& operator<<(const newrow_t);

      /**
       * Start a new sheet
       * @param  newsheet
       * @return the stream again.
       */
      spreadsheet_stream& operator<<(const newsheet_t& newsheet);

      /**
       * From now on, objects outputtet should not include details
       * @param brief
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const brief_t&) {
        brief(true);
        return *this;
      }

      /**
       * From now on, objects outputtet should include details
       * @param verbose
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const verbose_t&) {
        brief(false);
        return *this;
      }

      /**
       * Exit concat mode, go to the next cell if in concat mode
       * @param
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const endcell_t&) {
        concat(false);
        return *this;
      }

      /**
       * From now on, objects streamed are concatted to the current cell.
       * if already concatting to one cell, go to the next cell
       * @param
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const startcell_t&) {
        concat(true);
        return *this;
      }

      /**
       * Create a new style
       * @param ns
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const new_style& ns);

      /**
       * Set the style of new cells
       * @param ns
       * @return the stream again
       */
      spreadsheet_stream& operator<<(const spreadsheet::set_style& ss);

      /**
       * Output val as a cell, where val is converted to a text cell.
       * @param val
       * @return the stream again (for chaining)
       */
      template<typename T>
      spreadsheet_stream& operator<<(const T& val) {
        std::ostringstream os;
        os << val;
        return *this << os.str();
      }



  };

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
