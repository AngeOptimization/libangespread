//
// C++ Interface: region
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETREGION_H
#define SPREADSHEETREGION_H

class Sheet;

#include "sheet.h"

namespace spreadsheet {

/**
 A square region in a sheet, possibly the entire sheet.
 Regions are usually obtained from sheets and other regions.

  @author Ange Optimization <esben@ange.dk>
*/
class Region{
  public:
    typedef Sheet::iterator iterator;
    typedef Sheet::const_iterator const_iterator;
    typedef Sheet::Coord Coord;
  private:
    Coord m_row_offset;
    Coord m_col_offset;
    Coord m_row_length;
    Coord m_col_length;

    boost::shared_ptr<Sheet> m_sheet;

    Region(const Sheet& sheet);

    ~Region();

    Region row_region(Coord row);
    Region col_region(Coord col);
    Region square_region(Coord offset_row, Coord offset_col, Coord row_length, Coord col_length);

    iterator begin();
    iterator end();

    const_iterator begin() const;
    const_iterator end() const;

};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
