//
// C++ Implementation: csv_parser
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "csv_parser.h"
#include <fstream>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include "spreadsheet.h"
#include "sheet.h"
#include "cell.h"

#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_push_back_actor.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>
#include <boost/spirit/include/classic_iterator.hpp>

using namespace std;
using namespace boost::spirit::classic;

typedef ostream_iterator<string> sout; // it is just so convenient

namespace spreadsheet {

template<typename iterator_t>
bool parse_line(iterator_t first, iterator_t last, Sheet& sheet, Sheet::Coord row) {
    typedef scanner< iterator_t > scanner_t;
    typedef rule< scanner_t> rule_t;

    vector<string> output;
    // Make a rule that matches a string, and real or an int.
    rule_t list_csv_item =
        !(
                confix_p('\"', *anychar_p, '\"')
            |   *(((anychar_p - '\t')-'\"')|ch_p('+'))
           
        )[push_back_a(output)];

    // Expand this rule to a list of such items
    rule_t csv_list_rule = list_csv_item >> *('\t' >> list_csv_item);

    // Parse the line
    parse_info<iterator_t> result = parse (first, last, csv_list_rule);

    if (result.full) {
      for (unsigned column=0; column<output.size(); column++) {
        // cout << "ADding  \""  << output[column] << "\" to (" << column << ", " << row << ")" << endl;
        sheet(column, row) = output[column];
      }
    }

    // Return true on a full match
    return result.full;
}

template<typename iterator_t>
bool parse_tab_separator(iterator_t first, iterator_t last) {
    typedef scanner< iterator_t > scanner_t;
    typedef rule< scanner_t> rule_t;
    rule_t tab_sep_rule = str_p("New tab");
    parse_info<iterator_t> result = parse (first, last, tab_sep_rule);
    return result.full;
}

struct parse_sheet_action {
  parse_sheet_action(Spreadsheet& spreadsheet, Sheet::Coord& row, int& lineno)
     : spreadsheet(spreadsheet), row(row),lineno(lineno) {}

  template<typename IteratorT>
  void operator()(IteratorT first, IteratorT last) const {
    bool newtab = parse_tab_separator(first, last);
    if (spreadsheet.num_sheets()==0 || newtab) {
      spreadsheet.add_sheet();
      row = 0;
      lineno++;
    }
    if (newtab || parse_line(first, last, spreadsheet.get_sheet(spreadsheet.num_sheets()-1), row++)) {
      lineno++;
    } else {
      cerr << "Error while parsing fragment \"" << string(first,last) << "\" at line " << lineno << endl;
    }     
  }
  Spreadsheet& spreadsheet;
  Sheet::Coord&  row;
  int& lineno;
};

  template<typename iterator_t>
  bool parse_spreadsheet(iterator_t first, iterator_t last, Spreadsheet& spreadsheet) {
    
    Sheet::Coord  row = 0;
    int lineno = 0;
    
    typedef scanner< iterator_t > scanner_t;
    rule< scanner_t > list_csv_sheet = *(anychar_p - '\n');
    rule< scanner_t > nsep = +(str_p('\n'));
    rule< scanner_t > list_wo_sheet = *str_p('\n') >> list_p(list_csv_sheet[parse_sheet_action(spreadsheet,row,lineno)], nsep);
    parse_info<iterator_t > result = parse (first, last, list_wo_sheet);    
    return result.full;
  }

  std::auto_ptr<Spreadsheet> Csv_parser::parse_csv_file(const std::string& filename) {
    file_iterator<> first(filename.c_str());
    if (!first) {
    // open failed
      return std::auto_ptr<Spreadsheet>();
    }     
    file_iterator<> last = first.make_end();
    
    std::auto_ptr<Spreadsheet> spreadsheet(new Spreadsheet());
  
    if (!parse_spreadsheet(first,last,*spreadsheet))  {
      throw runtime_error("Failed to parse spreadsheet!");
    }    
    return spreadsheet;;
  }
}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
