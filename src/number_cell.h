//
// C++ Interface: number_cell
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETNUMBER_CELL_H
#define SPREADSHEETNUMBER_CELL_H

#include "cell.h"

namespace spreadsheet {

/**
  @author Ange Optimization <esben@ange.dk>
*/
class number_cell : public Cell
{
  public:
    /**
     * 
     * @param contenst contents of the new cell. Default is a blank cell.
     */
    number_cell(Cell_position cell_position,
                double contents = 0.0);

    number_cell(Cell_position cell_position,
                int contents = 0);
    number_cell(Cell_position cell_position,
                unsigned int contents = 0);
    /**
     * destroy this cell
     */
    virtual ~number_cell();

    virtual std::string text() const;
    virtual std::string formula() const;
    virtual double numeric() const { return m_contents; }
    virtual int integer(const int def) const;
    virtual std::ostream& write(std::ostream& stream) const;
    virtual boost::shared_ptr<Cell> copy(const Cell_position& cell_position) const {
      return boost::shared_ptr<Cell>(new number_cell(cell_position, m_contents));
    }
    type_t type() const { if(m_is_int) {  return type_int; } else { return type_float;} }

private:

  double m_contents;
  bool m_is_int;
};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
