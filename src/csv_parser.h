//
// C++ Interface: csv_parser
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPREADSHEETCSV_PARSER_H
#define SPREADSHEETCSV_PARSER_H

#include <memory>
#include <string>

namespace spreadsheet {
class  Spreadsheet;


/**
Creates spreadsheets from csv-files.

  @author Ange Optimization <esben@ange.dk>
*/
class Csv_parser 
{
public:

private:
    Csv_parser();

    ~Csv_parser();

public:
    static std::auto_ptr<Spreadsheet> parse_csv_file(const std::string& filename);
};

}

#endif
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
