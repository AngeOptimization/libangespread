//
// C++ Implementation: odf_writer
//
// Description: 
//
//
// Author: Ange Optimization <esben@ange.dk>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "odf_writer.h"
#include "spreadsheet.h"
#include "errors.h"
#include "zip_archive.h"
#include "sheet.h"
#include "cell.h"
#include "style.h"

#include <algorithm>
#include <functional>
#include <iomanip>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/foreach.hpp>


using namespace boost::lambda;
using namespace std;

namespace spreadsheet {

void odf_writer::add_metadata() {
    m_za.add_file("META-INF/manifest.xml", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                "<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\">\n"
                "<manifest:file-entry manifest:media-type=\"application/vnd.oasis.opendocument.spreadsheet\" manifest:full-path=\"/\"/>\n"
                "<manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"content.xml\"/>\n"
                "<manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"styles.xml\"/>\n"
                "</manifest:manifest>\n");
    m_za.add_file("mimetype", "application/vnd.oasis.opendocument.spreadsheet");
    m_za.add_file("styles.xml",
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                "  <office:document-styles\n"
                "  xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\"\n"
                "  xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\"\n"
                "  xmlns:config=\"urn:oasis:names:tc:opendocument:xmlns:config:1.0\"\n"
                "  xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\" \n"
                "  xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\" \n"
                "  xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\" "
                   "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
                "</office:document-styles>\n");
    m_os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                "<office:document-content xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\"\n"
                " xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\"\n"
                " xmlns:config=\"urn:oasis:names:tc:opendocument:xmlns:config:1.0\"\n"
                " xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\"\n"
                " xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\"\n"
                " xmlns:dr3d=\"urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0\"\n"
                " xmlns:style=\"urn:oasis:names:tc:opendocument:xmlns:style:1.0\"\n"
                " xmlns:number=\"urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0\"\n"
                " xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\"\n"
                " xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
                "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";
}


odf_writer::odf_writer(const std::string& filename, const Spreadsheet* spreadsheet)
 : m_filename(filename),
   m_spreadsheet(spreadsheet),
   m_za(zip_archive::create_overwrite( filename )),
   m_open(true)
{
  if (!spreadsheet) {
    throw error("spreadsheet was null");
  }
  add_metadata();
}


odf_writer::~odf_writer()
{
  close();
}

  std::string odf_writer::xml_escape(std::string text)  {

    //  quot "  U+0022 (34) (double) quotation mark
    //  amp  &  U+0026 (38) ampersand
    //  apos '  U+0027 (39) apostrophe (= apostrophe-quote)
    //  lt   <  U+003C (60) less-than sign
    //  gt   >  U+003E (62) greater-than sign

    ostringstream xml_text_os;
 
    for(unsigned int i = 0; i < text.length(); i++)
    {
      char c = text [i];

      switch (c) {
        case '"':
          xml_text_os << "&quot;";
          break;
        case '&':
          xml_text_os << "&amp;";
          break;
        case '\'':
          xml_text_os << "&apos;";
          break;
        case '<':
          xml_text_os << "&lt;";
          break;
        case '>':
          xml_text_os << "&gt;";
          break;
        default:
          xml_text_os << c;
      }
      
    }

    return xml_text_os.str();
  }
  

struct call_write_sheet {
  odf_writer& m_obj;
  call_write_sheet(odf_writer& obj) : m_obj(obj) {}
  void operator()( const Sheet& sheet ){ m_obj.write_sheet(sheet); }
};

void odf_writer::write_all() {
  m_os << "<office:automatic-styles>\n";
  // TODO: This is a temporary hack. Should only be the styles used in Cells.
  vector<string> style_names = m_spreadsheet->style_names();
  BOOST_FOREACH(const string& style_name, style_names) {
    boost::shared_ptr<style> style = m_spreadsheet->get_style( style_name );
    m_os <<  style->style_for(style::exporter_odf) << "\n";
  }
  m_os << " </office:automatic-styles>\n"
          " <office:body>\n"
          "  <office:spreadsheet>\n";

  for_each(m_spreadsheet->begin(), m_spreadsheet->end(), call_write_sheet(*this));
}

void odf_writer::write_cell(const Cell& cell,int count) {
  string rep = "";
  switch (cell.type()) {
    case Cell::type_string:
      if (cell.text().length()==0) {
        m_os << "<table:table-cell"<<rep;
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os <<"/>\n";
      } else {
        m_os << "<table:table-cell";
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os <<" office:value-type=\"string\"";
        if (!cell.style_name().empty()) {
          m_os << " table:style-name=\"" << cell.style_name() << "\"";
        }
        m_os <<  ">\n"
          "  <text:p>" << xml_escape(cell.text())
             <<  "</text:p>\n"
          "</table:table-cell>\n";
      }
      break;
    case Cell::type_float:
      if (!cell.text().compare("inf")) {
        m_os << "<table:table-cell"<<rep;
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os <<"/>\n";
      } else {
        m_os << "<table:table-cell";
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os << " office:value-type=\"float\" "
              "office:value=\"" << cell.text() << "\"";
        if (cell.style_name().empty()) {
          m_os << " table:style-name=\"cfloat\"" ;
        } else {
          m_os << " table:style-name=\"" << cell.style_name() << "\"";
        }
        m_os << ">\n"
          "  <text:p>" << cell.text()
             << "</text:p>\n"
          "</table:table-cell>\n";
      }
      break;
    case Cell::type_int:
      if (!cell.text().compare("inf")) {
        m_os << "<table:table-cell"<<rep;
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os <<"/>\n";
      } else {
        m_os << "<table:table-cell";
        if (count>1) {
          m_os << " table:number-columns-repeated=\"" << count << "\"";
        }        
        m_os << " office:value-type=\"float\" "
              "office:value=\"" << cell.text() << "\"";
        if (cell.style_name().empty()) {
          m_os << " table:style-name=\"cint\"" ;
        } else {
          m_os << " table:style-name=\"" << cell.style_name() << "\"";
        }
        m_os << ">\n"
          "  <text:p>" << cell.text()
             << "</text:p>\n"
          "</table:table-cell>\n";
      }
      break;
  }
}

void odf_writer::write_cells_as_row(Sheet::const_iterator begin, Sheet::const_iterator end) {
  m_os << "<table:table-row>\n";
  for (; begin!=end;) {
    int count = 0;
    const Cell &cell = *begin;
    do  {
      ++begin;      
      count++;
    } while (begin!=end && begin->type()==cell.type() && !begin->text().compare(cell.text()));
    write_cell(cell,count);
  }
  m_os << "</table:table-row>\n";
}

void odf_writer::write_sheet(const Sheet& sheet) {
  if (!m_open) {
    throw ioerror(m_filename, " already closed");
  }
    m_os << "<table:table table:name=\"" << sheet.name() << "\">\n";
  for (Sheet::const_iterator it = sheet.begin(), rowend=sheet.begin().next_row(), theend=sheet.end();
       it!=theend;
       it.next_row(), rowend.next_row())
  {
    write_cells_as_row(it, rowend);
  }
    m_os << "</table:table>\n";

}

void odf_writer::close() {
  if (m_open) {
    m_os << "  </office:spreadsheet>\n"
            "  </office:body>\n"
            "</office:document-content>\n";
    m_za.add_file("content.xml", m_os.str());
    m_za.close();
    m_open = false;
  }
}

}
/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 * vim: shiftwidth=2 tabstop=4 expandtab
 */
